import { Component, OnInit } from '@angular/core';
import { faChevronCircleRight} from '@fortawesome/free-solid-svg-icons';
import { DataService } from '../data.service';
import { News } from '../news.model';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
	faChevronCircleRight = faChevronCircleRight;
	items = [
		{
			text: 'Invoices'
		},
		{
			text: 'Payment Confirmation'
		}
	];
	news: News[] = [];

  constructor(
	  private dataService: DataService
  ) {
	  dataService.newsUpdate.subscribe(news => {
		  this.news = news;
	  });
  }

  ngOnInit(): void {
  }

}
