export interface Faq {
	_id?: string;
	sort: number;
	question: string;
	answer: string;
}
