import { Component, OnInit } from '@angular/core';
import { faTwitter, faKeybase, faGitlab } from '@fortawesome/free-brands-svg-icons';
import { faEnvelopeSquare, faComments } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
	faTwitter = faTwitter;
	faKeybase = faKeybase;
	faGitlab = faGitlab;
	faEnvelopeSquare = faEnvelopeSquare;
	faComments = faComments;

  constructor() { }

  ngOnInit(): void {
  }

}
