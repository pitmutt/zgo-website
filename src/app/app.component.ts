import { Component } from '@angular/core';
import { faCopyright } from '@fortawesome/free-regular-svg-icons';
import { faAngular, faNode } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'zgo-web';
  faCopyright = faCopyright;
  faAngular = faAngular;
  faNode = faNode;
}
