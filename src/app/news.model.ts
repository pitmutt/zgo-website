export interface News {
	_id?: string;
	title: string;
	date: string;
	content: string;
}
