import { Component, OnInit } from '@angular/core';
import { faChevronCircleRight} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-upcoming',
  templateUrl: './upcoming.component.html',
  styleUrls: ['./upcoming.component.css']
})
export class UpcomingComponent implements OnInit {
	faChevronCircleRight = faChevronCircleRight;
	items = [
		{
			text: 'New login mechanism.'
		},{
			text: 'Beta Testing Partner program.'
		},{
			text: 'GitLab repository for the main ZGo application.'
		},{
			text: 'Option to price items in zatoshis.'
		}
	];

  constructor() { }

  ngOnInit(): void {
  }

}
