import { Component, OnInit } from '@angular/core';
import { faExpandArrowsAlt, faMobileAlt, faQrcode, faShieldAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
	faExpandArrowsAlt = faExpandArrowsAlt;
	faMobileAlt = faMobileAlt;
	faQrcode = faQrcode;
	faShieldAlt = faShieldAlt;
	tiles = [
		{
			title: 'Non-custodial',
			icon: this.faShieldAlt,
			msg: 'The Zcash payment goes from your customer to your wallet through shielded transactions.'
		}, {
			title: 'Flexible',
			icon: this.faExpandArrowsAlt,
			msg: 'Need it for an afternoon? Multiple points of sale everyday? ZGo can handle it.' 
		}, {
			title: 'Mobile-ready',
			icon: this.faMobileAlt,
			msg: 'Desktop or mobile, you can have your shop where you need it.'
		}, {
			title: 'Compatible',
			icon: this.faQrcode,
			msg: 'Works with major Zcash wallets on mobile and desktop.'
		}
	];
	breakpoint = 2;

  constructor() { }

  ngOnInit(): void {
	  this.breakpoint = (window.innerWidth <= 500) ? 1 : 2;
  }
  
  onResize(event: any) {
	  this.breakpoint = (event.target.innerWidth <= 500) ? 1 : 2;
  }

}
