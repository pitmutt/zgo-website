import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Faq } from '../faq.model';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {
	questions: Faq[] = [];

  constructor(
	  private dataService: DataService
  ) { 
	  dataService.faqUpdate.subscribe(faqs => {
		  this.questions = faqs;
	  });
  }

  ngOnInit(): void {
  }

}
