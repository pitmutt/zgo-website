import { Injectable } from '@angular/core';
import {Subject, Subscription, BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';

import { News } from './news.model';
import { Faq }  from './faq.model';

@Injectable({providedIn: 'root'})

export class DataService {
	beUrl = 'http://localhost:3000/';
	private dataStore: { news: News[], faqs: Faq[]} = { news: [], faqs: [] };
	private _newsUpdated: BehaviorSubject<News[]> = new BehaviorSubject(this.dataStore.news);
	private _faqUpdated: BehaviorSubject<Faq[]> = new BehaviorSubject(this.dataStore.faqs);
	public readonly newsUpdate = this._newsUpdated.asObservable();
	public readonly faqUpdate = this._faqUpdated.asObservable();

	
	private apiKey = 'Your_API_Key';
	private reqHeaders: HttpHeaders;

	constructor(
		private http: HttpClient,
	) {
		this.reqHeaders = new HttpHeaders().set('Authorization', this.apiKey);
		this.getNews();
		this.getFaqs();
	}

	getNews(){
		let obs = this.http.get<{message: string, news?: any}>(this.beUrl+'api/news', { headers: this.reqHeaders, observe: 'response'});
		obs.subscribe(NewsDataResponse => {
			if (NewsDataResponse.status == 200) {
				this.dataStore.news = NewsDataResponse.body!.news;
				this._newsUpdated.next(Object.assign({}, this.dataStore).news);
			} else {
				console.log('No news found');
				this._newsUpdated.next([]);
			}
		});
	}

	getFaqs(){
		let obs = this.http.get<{message: string, faqs?: any}>(this.beUrl+'api/faq', { headers: this.reqHeaders, observe: 'response'});
		obs.subscribe(FaqDataResponse => {
			if (FaqDataResponse.status == 200) {
				this.dataStore.faqs = FaqDataResponse.body!.faqs;
				this._faqUpdated.next(Object.assign({}, this.dataStore).faqs);
			} else {
				console.log('No questions found');
				this._faqUpdated.next([]);
			}
		});
	}
}
