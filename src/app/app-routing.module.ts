import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { FaqComponent} from './faq/faq.component';
import { DemoComponent} from './demo/demo.component';
import { GuideComponent } from './guide/guide.component';
import { ContactComponent } from './contact/contact.component';
import { NewsComponent } from './news/news.component';
import { InfoComponent } from './info/info.component';
import { PartnersComponent } from './partners/partners.component';

const routes: Routes = [
	{ path:'about', component: AboutComponent},
	{ path: '', component: AboutComponent},
	{ path:'guide', component: GuideComponent},
	{ path:'demo', component: DemoComponent},
	{ path:'contact', component: ContactComponent },
	{ path:'faq', component: FaqComponent},
	{ path:'news', component: NewsComponent },
	{ path:'info', component: InfoComponent },
	{ path:'partners', component: PartnersComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
