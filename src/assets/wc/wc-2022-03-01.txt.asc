-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Warrant Canary, March 1st 2022

1. We have not placed any backdoors into zgo.cash or app.zgo.cash software, and we have not complied with any requests to do so.
2. We have not complied with any National Security Letters or FISA court orders regarding our software.
3. We have not been subject to any gag order by a FISA court regarding our software.

HackerNews Front Page Top 20 (https://news.ycombinator.com/)
1.	
How generics are implemented in Go 1.18 (github.com/golang)
62 points by komuW 1 hour ago | hide | 2 comments
2.	
RISC-V is succeeding (semiengineering.com)
227 points by PaulHoule 5 hours ago | hide | 134 comments
3.	
David Boggs has died (nytimes.com)
208 points by rbanffy 5 hours ago | hide | 37 comments
4.	
Google Maps is used to coordinate air strikes in Ukraine (support.google.com)
101 points by ukrwantpiece 3 hours ago | hide | 50 comments
5.	
Potato farmers conquer a devastating worm with paper made from bananas (science.org)
133 points by rbanffy 5 hours ago | hide | 15 comments
6.	
The Road to Success Is Paved with Rejection Letters (perceiving-systems.blog)
51 points by cloudedcordial 2 hours ago | hide | 23 comments
7.	
Launch HN: GrowthBook (YC W22) – Open-source feature flagging and A/B testing (growthbook.io)
78 points by combat_wombat 3 hours ago | hide | 23 comments
8.	
Flow Browser: Flow makes HTML faster (ekioh.com)
122 points by ksec 5 hours ago | hide | 32 comments
9.	
Debugging with GDB (felix-knorr.net)
82 points by MarcellusDrum 4 hours ago | hide | 26 comments
10.	
DDoS attacks slow down Citizens' Initiative signing in Finland
56 points by ptaipale 1 hour ago | hide | 6 comments
11.	
Things you don't need JavaScript for (lexoral.com)
291 points by StevenWaterman 9 hours ago | hide | 174 comments
12.	
How much do founders pay themselves? A European data set (sifted.eu)
141 points by vinnyglennon 6 hours ago | hide | 79 comments
13.		Emerge Tools (YC W21) is hiring a senior Android engineer (workatastartup.com)
3 hours ago | hide
14.	
Ask HN: Who is hiring? (March 2022)
194 points by whoishiring 4 hours ago | hide | 437 comments
15.	
An insane baseball proposal: Dual league restructuring (xstats.org)
73 points by SubiculumCode 2 hours ago | hide | 50 comments
16.	
Mozilla Hubs (hubs.mozilla.com)
221 points by amar-laksh 5 hours ago | hide | 121 comments
17.	
Ask HN: Who wants to be hired? (March 2022)
56 points by whoishiring 4 hours ago | hide | 105 comments
18.	
Huawei MatePad Paper – eInk Tablet (huawei.com)
224 points by gadders 9 hours ago | hide | 227 comments
19.	
The Supernatural Sheep of Slovenia’s Door-to-Door Carnival (atlasobscura.com)
10 points by Thevet 1 hour ago | hide | 1 comment
20.	
MDN Redesign (hacks.mozilla.org)
217 points by Vilkku 6 hours ago | hide | 119 comments
-----BEGIN PGP SIGNATURE-----

iIUEARYIAC0WIQTbMTaUjmknB/o9Xd/pbtWszWYKKwUCYh6DnA8cYWRtaW5Aemdv
LmNhc2gACgkQ6W7VrM1mCiv/qwEA6eC02VrqhYHeFGV5JRw6dpy9rbBEHiDMkkKZ
HXFZtiMBAISTE3ZKSKE3RVLaKEycvf2k3kz1ZFviofzwkZunqdwA
=PIww
-----END PGP SIGNATURE-----
