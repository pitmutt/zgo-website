-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Warrant Canary, June 1st, 2022.

1. We have not placed any backdoors into zgo.cash or app.zgo.cash software, and we have not complied with any requests to do so.
2. We have not complied with any National Security Letters or FISA court orders regarding our software.
3. We have not been subject to any gag order by a FISA court regarding our software.

The next canary will be posted on July 1st, 2022.

Details for Zcash Block #1688149:

- - Hash: 00000000007955028e1f936abe8896bfd1e31aa042a15c1b66765df55ababcce
- - Mined: 2022-06-01 15:34:09
- - Transactions:
  - 17c35fea5fe2ba16e46c3487b23937e24450b5c6d6e594931a706a1a4e6711ac
  - 925611ca2c6db183ff48824ca3eae6a2cefe67bb4b47c06a1cb20cdc9bf2afe5
  - e3eb33abc459833b2322e3204cf491a7a94d01ca189c0f173bff0ac3c4d6220b
  - b31ee1526c6c41cb9f1504bc190a239a0342c46c83dd30ab8687fff0a7c1cdc5
  - a66c1855db83bda6c1e02c8ced07ad6498f86830d170d4671dd689eefaffa121
  - 0f44c169ace09eb1d18239640db6cdf6fa1d384021de3a26cc80422944bc3088
-----BEGIN PGP SIGNATURE-----

iHUEARYIAB0WIQTbMTaUjmknB/o9Xd/pbtWszWYKKwUCYpeJ2AAKCRDpbtWszWYK
K3lMAQDclVtEr2JAqhS9+5lcj8w9AZwRM9D5BthmuBfVb/bj/wEAmVZSlZUrAuVb
IzL44vTKLWGr+cEnCjkC675XmoKx3wo=
=jklH
-----END PGP SIGNATURE-----
