const express = require('express');
const app = express();
const bodyparser = require('body-parser');
const cors = require('cors');
const crypto = require('crypto');
const mongoose = require('mongoose');

const newsmodel = require('./models/news');
const faqsmodel = require('./models/faq.js');

var db = require('./config/db');
mongoose.connect('mongodb://'+db.user+':'+db.password+'@'+db.server+'/'+db.database).then(() => {
	console.log("connecting-- ", db.database);
}).catch(() => {
	console.log("connection failed!");
});

app.use(cors());
app.options('*', cors());

app.use(bodyparser.json());

app.use((req, res, next) => {
	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, OPTIONS");
	next();
});

app.use((req, res, next) => {
	if (req.headers.authorization !== 'TuFaiGhazeis2Ge1waij8uz2' ) {
		return res.status(401).send('Authorization required.');
	} else {
		next();
	}
});

app.get('/api/news', (req, res, next) => {
	newsmodel.find({}).sort([['date', -1]]).limit(5).then((documents) => {
		if (documents != null) {
			res.status(200).json({
				message: 'News found',
				news: documents
			});
		} else {
			res.status(204).json({
				message: 'No news found'
			});
		}
	});
});

app.get('/api/faq', (req, res, next) =>{
	faqsmodel.find().sort({rank: "asc"}).then((documents) => {
		if (documents != null) {
			res.status(200).json({
				message: 'FAQs found',
				faqs: documents
			});
		} else {
			res.status(204).json({
				message: 'No data found'
			});
		}
	});
});

module.exports = app;
