const mongoose = require('mongoose');

const newsSchema = mongoose.Schema({
	title: {type: String, required: true},
	date: {type: Date, required:true, default: Date.now},
	content: {type: String, required: true}
});

module.exports = mongoose.model('News', newsSchema);
