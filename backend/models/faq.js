const mongoose = require('mongoose');

const faqSchema = mongoose.Schema({
	rank: {type: Number, required: true},
	question: {type: String, required: true},
	answer: {type: String, required: true}
});

module.exports = mongoose.model('FAQ', faqSchema);
