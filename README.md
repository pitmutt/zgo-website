# ZGo.cash Website

This repo houses the code behind the website for [ZGo](https://zgo.cash/).

## Dependencies

- NodeJS
- AngularJS
- Angular Material
- MongoDB


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.8.

