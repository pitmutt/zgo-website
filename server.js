var fs = require('fs');
var http = require('http');
//var https = require('https');

//var certificate = fs.readFileSync('/path/to/your/cert/fullchain.pem');
//var privateKey = fs.readFileSync('/path/to/your/cert/privkey.pem');

//var credentials = {key: privateKey, cert: certificate};

const app = require('./backend/app');
const port = 3000;

app.set('port', port);

const httpServer = http.createServer(app);
//const httpsServer = https.createServer(credentials, app);

httpServer.listen(port);
//httpsServer.listen(port);
